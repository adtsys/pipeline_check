namespace :pipeline do
  desc ""
  task monitor: :environment do

    Process.daemon(true, true)
    File.open('pipeline_monitor.pid', 'w') { |f| f << Process.pid }

    require "libnotify" if /linux/ =~ RUBY_PLATFORM
    require "terminal-notifier" if /darwin/ =~ RUBY_PLATFORM

    pipelines = {}

    notifier = PipelineMonitor::Notifier.new
    Signal.trap('TERM') { abort }
    Rails.logger.info "Start daemon..."

    loop do
      sleep_time = 10

      Pipeline.where(done: false).each do |pipeline|
        sleep_time = 2.5
        if pipelines[pipeline.id].blank?
          total_steps = pipeline.steps.count
          steps = pipeline.steps.order(priority: :asc).map { |step| [ step.id, false ] }.to_h
          actual_step = PipelineStep.find(steps.keys.first)
          pipelines[pipeline.id] = { object: pipeline, total_steps: total_steps, steps: steps }
        else
          total_steps = pipelines[pipeline.id][:total_steps]
          steps = pipelines[pipeline.id][:steps]
          not_checked_steps = steps.reject { |step, value| value == true }
          actual_step = PipelineStep.find(not_checked_steps.keys.first)
        end

        if actual_step.done?

          notifier.notify(
            "Pipeline #{pipeline.pipeline_type}",
            "Pipeline #{pipeline.id}\nStep[#{actual_step.id}] #{actual_step.priority+1}/#{total_steps} terminou!",
            "emblem-ok-symbolic",
            :low,
            5
          )

          sleep_time = 0.5
          steps[actual_step.id] = true
        end

        if actual_step.failed?
          notifier.notify(
            "Pipeline #{pipeline.pipeline_type}",
            "Pipeline #{pipeline.id} falhou\nStep[#{actual_step.id}] #{actual_step.priority+1}/#{total_steps}",
            "emblem-important-symbolic",
            :critical,
            5
          )
        end
      end

      # Finished pipelines check
      pipelines.values.each do |pipeline|
        pipeline = pipeline[:object].reload
        if pipeline.done?
          notifier.notify(
            "Pipeline terminou",
            "Pipeline #{pipeline.id} TERMINOU!",
            "emblem-favorite-symbolic",
            :normal,
            10
          )

          pipelines.delete(pipeline.id)
          sleep_time = ENV['INTERVAL'] || 0.5
        end
      end

      sleep(sleep_time)
    end
  end
end
