# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'pipeline_monitor/version'

Gem::Specification.new do |spec|
  spec.name          = "pipeline_monitor"
  spec.version       = PipelineMonitor::VERSION
  spec.authors       = ["Luiz Thomaz"]
  spec.email         = ["luiztajr@gmail.com"]
  spec.summary       = %q{Pipeline monitor}
  spec.description   = %q{Monitoring pipeline executions}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_dependency "rails", ">=4.0"
  spec.add_runtime_dependency "terminal-notifier", "1.6.3"
  spec.add_runtime_dependency "libnotify", "0.9.1"
end
